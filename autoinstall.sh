#!/bin/bash

#This is an auto install program that installs all the programs I want.
INSTALL='sudo pacman -S'
UPDATE='sudo pacman -Syu'
AUR='yay -S'

$UPDATE
$INSTALL mpv feh redshift linux-firmware-qlogic pavucontrol picom nitrogen thunar gvfs lxappearance
qt5ct breeze breeze-gtk redshift htop lsb-release libreoffice-fresh python-psutil ufws scrot
keepassxc rofi polybar

#UFW Setup
sudo systemctl enable ufw | sudo systemctl start ufw | sudo ufw allow 22/tcp | sudo ufw allow 80/tcp | sudo ufw allow 443/tcp 
echo "UFW setup is done"

#Synth shell, needs manual install
cd
git clone --recursive https://github.com/andresgongora/synth-shell.git
cd synth-shell
./setup.sh
cp ~/Qtile-conf/synth-shell/synth-shell-prompt.config ~/.config/synth-shell/synth-shell-prompt.config

#Fakefetch setup
sudo pacman -S lsb-release
cd
git clone https://gitlab.com/Shipwreckt/fakefetch
cd fakefetch
chmod +x fakefetch.sh
sudo mv ~/fakefetch/fakefetch.sh /bin/fakefetch
echo "fakefetch" >> ~/.bashrc

#Picom config file setup
sudo cp /etc/xdg/picom.conf ~/.config/picom.conf
sudo chown $(whoami):users ~/.config/picom.conf

#YAY install, for AUR
sudo pacman -S --needed git
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay

#Firmware from AUR
$AUR wd719x-firmware
$AUR -S upd72020x-fw

#Misc AUR
$AUR yt-dlp-drop-in librewolf-bin nuclear-player-bin 

echo "Done installing!"
