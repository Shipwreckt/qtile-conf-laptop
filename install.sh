#!/bin/bash

#Variables
INSTALL='sudo pacman -S --noconfirm'

#The reason why git is here is because it is a useful tool
sudo pacman -Syu --noconfirm git

#----------------------------------------------------------------------------------------------#
read -p "Do you want yay? (will need for firmware) (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    if ! command -v yay &> /dev/null; then
        sudo pacman -S --needed git
        git clone https://aur.archlinux.org/yay.git
        cd yay
        makepkg -si
        cd ..
        rm -rf yay
    else
        echo "yay is already installed."
    fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install polybar and rofi? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL polybar rofi 
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install mpv, feh, and scrot ? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL mpv feh scrot
    yay -S yt-dlp-drop-in #This makes it possable to watch youtube videos in mpv 
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install Nuclear music player? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	yay -S nuclear-player-bin 
fi
#----------------------------------------------------------------------------------------------#read -p "Do you want to install keepassxc? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL keepassxc
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install steam? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL steam
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install and set up redshift? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	cp configs/redshift.conf ~/.config/redshift.conf
	$INSTALL redshift
fi
#----------------------------------------------------------------------------------------------#
read -p "Qtile config? (y/n) " install_choice

if [ "$install_choice" = "y" ]; then
	cp configs/config.py ~/.config/qtile/config.py
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install linux firmware? (y/n) " install_choice

if [ "$install_choice" = "y" ]; then
    sudo pacman -S --noconfirm linux-firmware-qlogic
    yay -S wd719x-firmware
    yay -S upd72020x-fw
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install librewolf? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    yay -S librewolf-bin
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install pavucontrol? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL pavucontrol
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install picom? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    sudo pacman -S --noconfirm picom
    sudo cp configs/picom.conf ~/.config/picom.conf
    sudo chown $(whoami):users ~/.config/picom.conf
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install nitrogen? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    sudo pacman -S --noconfirm nitrogen
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install thunar file manager? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL thunar gvfs
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install synth shell? (y/n): " install_choice


if [ "$install_choice" = "y" ]; then
	cd
	git clone --recursive https://github.com/andresgongora/synth-shell.git
	cd synth-shell
	./setup.sh
	cp configs/synth-shell/synth-shell-prompt.config ~/.config/synth-shell/synth-shell-prompt.config
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install some themes? (lxappearance qt5ct breeze breeze-gtk)(y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL lxappearance qt5ct breeze breeze-gtk
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install redshift? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	$INSTALL redshift
    cp configs/redshift.conf
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install Htop? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	$INSTALL htop
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install fakefetch? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	sudo pacman -S lsb-release
    	cd
       	git clone https://gitlab.com/Shipwreckt/fakefetch 
	cd fakefetch
	chmod +x fakefetch.sh
    	sudo mv ~/fakefetch/fakefetch.sh /bin/fakefetch
	echo "fakefetch" >> ~/.bashrc
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install Vim? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	$INSTALL vim
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install libre office? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	$INSTALL libreoffice-fresh
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install psutil? (for qtile CPU) (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
	$INSTALL python-psutil
fi
#----------------------------------------------------------------------------------------------#
read -p "Do you want to install and set up ufw? (y/n): " install_choice

if [ "$install_choice" = "y" ]; then
    $INSTALL ufw
    sudo systemctl enable ufw
    sudo systemctl start ufw

    #Allows all your favourite stuff
    sudo ufw allow 22/tcp     # SSH
    sudo ufw allow 80/tcp     # HTTP
    sudo ufw allow 443/tcp    # HTTPS, kinda have to keep this one

    #ufw enable
    sudo ufw --force enable   # --force is used to avoid interactive prompt
    sudo ufw status
fi
echo '----------------------------------------------------------------------------------------------'
echo 'All is done enjoy your programs!'
echo '----------------------------------------------------------------------------------------------'
